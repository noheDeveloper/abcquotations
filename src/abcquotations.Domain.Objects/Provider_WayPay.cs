//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace abcquotations.Domain.Objects
{
    using System;
    using System.Collections.Generic;
    
    public partial class Provider_WayPay
    {
        public int IdClient_WayPay { get; set; }
        public int IdProvider { get; set; }
        public int IdWayPay { get; set; }
        public int RowCreatedSYSUserID { get; set; }
        public Nullable<System.DateTime> RowCreatedDateTime { get; set; }
        public int RowModifiedSYSUserID { get; set; }
        public Nullable<System.DateTime> RowModifiedDateTime { get; set; }
    
        public virtual Provider Provider { get; set; }
        public virtual WayPay WayPay { get; set; }
    }
}
