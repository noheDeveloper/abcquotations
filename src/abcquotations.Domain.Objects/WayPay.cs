//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace abcquotations.Domain.Objects
{
    using System;
    using System.Collections.Generic;
    
    public partial class WayPay
    {
        public WayPay()
        {
            this.Client_WayPay = new HashSet<Client_WayPay>();
            this.Provider_WayPay = new HashSet<Provider_WayPay>();
        }
    
        public int IdWayPay { get; set; }
        public string Days { get; set; }
        public string Description { get; set; }
        public int RowCreatedSYSUserID { get; set; }
        public Nullable<System.DateTime> RowCreatedDateTime { get; set; }
        public Nullable<int> RowModifiedSYSUserID { get; set; }
        public Nullable<System.DateTime> RowModifiedDateTime { get; set; }
    
        public virtual ICollection<Client_WayPay> Client_WayPay { get; set; }
        public virtual ICollection<Provider_WayPay> Provider_WayPay { get; set; }
    }
}
