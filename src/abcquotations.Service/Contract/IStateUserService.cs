﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using abcquotations.Domain.Entities;

namespace abcquotations.Service.Contract
{
    public interface IStateUserService
    {
        void Add(StateUser stateUser);
        void Update(StateUser stateUser);
        void Delete(StateUser stateUser);
        IEnumerable<StateUser> GetAll();
        StateUser GetById(int id);
        StateUser FindT(Expression<Func<StateUser, bool>> predicad);
    }
}
