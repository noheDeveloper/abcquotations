﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using abcquotations.Domain.Entities;


namespace abcquotations.Service.Contract
{
    public interface IWayPayService
    {
        void Add(WayPay wayPay);
        void Update(WayPay wayPay);
        void Delete(WayPay wayPay);
        IEnumerable<WayPay> GetAll();
        WayPay GetById(int id);
        WayPay FindT(Expression<Func<WayPay, bool>> predicad);
    }

}
