﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using abcquotations.Domain.Entities;

namespace abcquotations.Service.Contract
{
    public interface IClientService
    {
        void Add(Client client);
        void Update(Client client);
        void Delete(Client client);
        IEnumerable<Client> GetAll();
        Client GetById(int id);
        Client FindT(Expression<Func<Client, bool>> predicad);
    }
}
