﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using abcquotations.Domain.Entities;

namespace abcquotations.Service.Contract
{
    public interface ITypeDocumentService
    {
        void Add(TypeDocument typeDocument);
        void Update(TypeDocument typeDocument);
        void Delete(TypeDocument typeDocument);
        IEnumerable<TypeDocument> GetAll();
        TypeDocument GetById(int id);
        TypeDocument FindT(Expression<Func<TypeDocument, bool>> predicad);
    }
}
