﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using abcquotations.Domain.Entities;


namespace abcquotations.Service.Contract
{
    public interface IProviderService
    {
        void Add(Provider provider);
        void Update(Provider provider);
        void Delete(Provider provider);
        IEnumerable<Provider> GetAll();
        Provider GetById(int id);
        Provider FindT(Expression<Func<Provider, bool>> predicad);
    }
}
