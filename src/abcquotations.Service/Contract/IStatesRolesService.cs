﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using abcquotations.Domain.Entities;

namespace abcquotations.Service.Contract
{
    public interface IStatesRolesService
    {
        void Add(StateRol stateRol);
        void Update(StateRol stateRol);
        void Delete(StateRol stateRol);
        IEnumerable<StateRol> GetAll();
        StateRol GetById(int id);
        StateRol FindT(Expression<Func<StateRol, bool>> predicad);
    }
}
