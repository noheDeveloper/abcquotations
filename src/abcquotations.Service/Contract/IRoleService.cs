﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using abcquotations.Domain.Entities;

namespace abcquotations.Service.Contract
{
    public interface IRoleService
    {
        void Add(Role user);
        void Update(Role user);
        void Delete(Role user);
        IEnumerable<Role> GetAll();
        Role GetById(int id);
        Role FindT(Expression<Func<Role, bool>> predicad);
    }
}
