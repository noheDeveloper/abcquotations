﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using abcquotations.Domain.Entities;

namespace abcquotations.Service.Contract
{
    public interface IContactService
    {
        void Add(Contact contact);
        void Update(Contact contact);
        void Delete(Contact contact);
        IEnumerable<Contact> GetAll();
        Contact GetById(int id);
        Contact FindT(Expression<Func<Contact, bool>> predicad);
    }
}
