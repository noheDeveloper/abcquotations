﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using abcquotations.Domain.Entities;

namespace abcquotations.Service.Contract
{
    public interface IGenderService
    {
        void Add(Gender gender);
        void Update(Gender gender);
        void Delete(Gender gender);
        IEnumerable<Gender> GetAll();
        Gender GetById(int id);
        Gender FindT(Expression<Func<Gender, bool>> predicad);
    }
}
