﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using abcquotations.Domain.Entities;

namespace abcquotations.Service.Contract
{
    public interface IUserRoleService
    {
        void Add(UserRole userRole);
        void Update(UserRole userRole);
        void Delete(UserRole userRole);
        IEnumerable<UserRole> GetAll();
        UserRole GetById(int id);
        UserRole FindT(Expression<Func<UserRole, bool>> predicad);
    }
}
