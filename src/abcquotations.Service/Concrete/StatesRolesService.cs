﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using abcquotations.Domain.Entities;
using abcquotations.repository.Concrete;
using abcquotations.Service.Contract;

namespace abcquotations.Service.Concrete
{
    public class StatesRolesService : IStatesRolesService
    {
        private readonly Repository<StateRol> _repository = new Repository<StateRol>(new QuotationABCEntities());
        public void Add(StateRol stateRol)
        {
            _repository.Add(stateRol);
            _repository.Save();
        }

        public void Delete(StateRol stateRol)
        {
            _repository.Delete(stateRol);
            _repository.Save();
        }

        public StateRol FindT(Expression<Func<StateRol, bool>> predicad)
        {
            return _repository.FindT(predicad);
        }

        public IEnumerable<StateRol> GetAll()
        {
            var list = _repository.GetAll().ToList();
            return list.Any() ? list : new List<StateRol>();
        }

        public StateRol GetById(int id)
        {
            return _repository.FindById(id);
        }

        public void Update(StateRol stateRol)
        {
            _repository.Update(stateRol);
            _repository.Save();
        }
    }
}
