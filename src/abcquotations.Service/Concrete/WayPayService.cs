﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using abcquotations.Domain.Entities;
using abcquotations.repository.Concrete;
using abcquotations.Service.Contract;

namespace abcquotations.Service.Concrete
{
    public class WayPayService : IWayPayService
    {
        private readonly Repository<WayPay> _repository = new Repository<WayPay>(new QuotationABCEntities());
        public void Add(WayPay wayPay)
        {
            _repository.Add(wayPay);
            _repository.Save();
        }

        public void Update(WayPay wayPay)
        {
            _repository.Update(wayPay);
            _repository.Save();
        }

        public void Delete(WayPay wayPay)
        {
            _repository.Delete(wayPay);
            _repository.Save();
        }

        public IEnumerable<WayPay> GetAll()
        {
            var list = _repository.GetAll().ToList();
            return list.Any() ? list : new List<WayPay>();
        }

        public WayPay GetById(int id)
        {
            return _repository.FindById(id);
        }

        public WayPay FindT(Expression<Func<WayPay, bool>> predicad)
        {
            return _repository.FindT(predicad);
        }
    }
}
