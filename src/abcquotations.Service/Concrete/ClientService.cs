﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using abcquotations.Domain.Entities;
using abcquotations.repository.Concrete;
using abcquotations.Service.Contract;

namespace abcquotations.Service.Concrete
{
    public class ClientService : IClientService
    {
        private readonly Repository<Client> _repository = new Repository<Client>(new QuotationABCEntities());
        public void Add(Client client)
        {
            _repository.Add(client);
            _repository.Save();
        }

        public void Update(Client client)
        {
            _repository.Update(client);
            _repository.Save();
        }

        public void Delete(Client client)
        {
            _repository.Delete(client);
            _repository.Save();
        }

        public IEnumerable<Client> GetAll()
        {
            var list = _repository.GetAll().ToList();
            return list.Any() ? list : new List<Client>();
        }

        public Client GetById(int id)
        {
            return _repository.FindById(id);
        }

        public Client FindT(Expression<Func<Client, bool>> predicad)
        {
            return _repository.FindT(predicad);
        }
    }
}
