﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using abcquotations.Domain.Entities;
using abcquotations.repository.Concrete;
using abcquotations.Service.Contract;

namespace abcquotations.Service.Concrete
{
    public class StateUserService : IStateUserService
    {
        private readonly Repository<StateUser> _repository = new Repository<StateUser>(new QuotationABCEntities());
        public void Add(StateUser user)
        {
            _repository.Add(user);
            _repository.Save();
        }

        public void Update(StateUser user)
        {
            _repository.Update(user);
            _repository.Save();
        }

        public void Delete(StateUser user)
        {
            _repository.Delete(user);
            _repository.Save();
        }

        public IEnumerable<StateUser> GetAll()
        {
            var list = _repository.GetAll().ToList();
            return list.Any() ? list : new List<StateUser>();
        }

        public StateUser GetById(int id)
        {
            return _repository.FindById(id);
        }

        public StateUser FindT(Expression<Func<StateUser, bool>> predicad)
        {
            return _repository.FindT(predicad);
        }
    }
}
