﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using abcquotations.Domain.Entities;
using abcquotations.repository.Concrete;
using abcquotations.Service.Contract;

namespace abcquotations.Service.Concrete
{
    public class ProviderService : IProviderService
    {
        private readonly Repository<Provider> _repository = new Repository<Provider>(new QuotationABCEntities());
        public void Add(Provider provider)
        {
            _repository.Add(provider);
            _repository.Save();
        }

        public void Delete(Provider provider)
        {
            _repository.Delete(provider);
            _repository.Save();
        }

        public Provider FindT(Expression<Func<Provider, bool>> predicad)
        {
            return _repository.FindT(predicad);
        }

        public IEnumerable<Provider> GetAll()
        {
            var list = _repository.GetAll().ToList();
            return list.Any() ? list : new List<Provider>();
        }

        public Provider GetById(int id)
        {
            return _repository.FindById(id);
        }

        public void Update(Provider provider)
        {
            _repository.Update(provider);
            _repository.Save();
        }
    }
}
