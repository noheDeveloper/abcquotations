﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using abcquotations.Domain.Entities;
using abcquotations.repository.Concrete;
using abcquotations.Service.Contract;

namespace abcquotations.Service.Concrete
{
    public class ContactService : IContactService
    {
        private readonly Repository<Contact> _repository = new Repository<Contact>(new QuotationABCEntities());
        public void Add(Contact contact)
        {
            _repository.Add(contact);
            _repository.Save();
        }

        public void Delete(Contact contact)
        {
            _repository.Delete(contact);
            _repository.Save();
        }

        public Contact FindT(Expression<Func<Contact, bool>> predicad)
        {
            return _repository.FindT(predicad);
        }

        public IEnumerable<Contact> GetAll()
        {
            var list = _repository.GetAll().ToList();
            return list.Any() ? list : new List<Contact>();
        }

        public Contact GetById(int id)
        {
            return _repository.FindById(id);
        }

        public void Update(Contact role)
        {
            _repository.Update(role);
            _repository.Save();
        }
    }
}
