﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using abcquotations.Domain.Entities;
using abcquotations.repository.Concrete;
using abcquotations.Service.Contract;

namespace abcquotations.Service.Concrete
{
    public class UserRoleService : IUserRoleService
    {
        private readonly Repository<UserRole> _repository = new Repository<UserRole>(new QuotationABCEntities());
        public void Add(UserRole userRole)
        {
            _repository.Add(userRole);
            _repository.Save();
        }

        public void Update(UserRole userRole)
        {
            _repository.Update(userRole);
            _repository.Save();
        }

        public void Delete(UserRole userRole)
        {
            _repository.Delete(userRole);
            _repository.Save();
        }

        public IEnumerable<UserRole> GetAll()
        {
            var list = _repository.GetAll().ToList();
            return list.Any() ? list : new List<UserRole>();
        }

        public UserRole GetById(int id)
        {
            return _repository.FindById(id);
        }

        public UserRole FindT(Expression<Func<UserRole, bool>> predicad)
        {
            return _repository.FindT(predicad);
        }
    }
}
