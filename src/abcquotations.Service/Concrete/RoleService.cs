﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using abcquotations.Domain.Entities;
using abcquotations.repository.Concrete;
using abcquotations.Service.Contract;

namespace abcquotations.Service.Concrete
{
    public class RoleService : IRoleService
    {
        private readonly Repository<Role> _repository = new Repository<Role>(new QuotationABCEntities());
        public void Add(Role role)
        {
            _repository.Add(role);
            _repository.Save();
        }

        public void Delete(Role role)
        {
            _repository.Delete(role);
            _repository.Save();
        }

        public Role FindT(Expression<Func<Role, bool>> predicad)
        {
            return _repository.FindT(predicad);
        }

        public IEnumerable<Role> GetAll()
        {
            var list = _repository.GetAll().ToList();
            return list.Any() ? list : new List<Role>();
        }

        public Role GetById(int id)
        {
            return _repository.FindById(id);
        }

        public void Update(Role role)
        {
            _repository.Update(role);
            _repository.Save();
        }
    }
}
