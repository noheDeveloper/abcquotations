﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using abcquotations.Domain.Entities;
using abcquotations.repository.Concrete;
using abcquotations.Service.Contract;

namespace abcquotations.Service.Concrete
{
    public class TypeDocumentService : ITypeDocumentService
    {
        private readonly Repository<TypeDocument> _repository = new Repository<TypeDocument>(new QuotationABCEntities());
        public void Add(TypeDocument typeDocument)
        {
            _repository.Add(typeDocument);
            _repository.Save();
        }

        public void Delete(TypeDocument typeDocument)
        {
            _repository.Delete(typeDocument);
            _repository.Save();
        }

        public TypeDocument FindT(Expression<Func<TypeDocument, bool>> predicad)
        {
            return _repository.FindT(predicad);
        }

        public IEnumerable<TypeDocument> GetAll()
        {
            var list = _repository.GetAll().ToList();
            return list.Any() ? list : new List<TypeDocument>();
        }

        public TypeDocument GetById(int id)
        {
            return _repository.FindById(id);
        }

        public void Update(TypeDocument role)
        {
            _repository.Update(role);
            _repository.Save();
        }
    }
}
