﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using abcquotations.Domain.Entities;
using abcquotations.repository.Concrete;
using abcquotations.Service.Contract;

namespace abcquotations.Service.Concrete
{
    public class GenderService : IGenderService
    {
        private readonly Repository<Gender> _repository = new Repository<Gender>(new QuotationABCEntities());
        public void Add(Gender gender)
        {
            _repository.Add(gender);
            _repository.Save();
        }

        public void Update(Gender gender)
        {
            _repository.Update(gender);
            _repository.Save();
        }

        public void Delete(Gender gender)
        {
            _repository.Delete(gender);
            _repository.Save();
        }

        public IEnumerable<Gender> GetAll()
        {
            var list = _repository.GetAll().ToList();
            return list.Any() ? list : new List<Gender>();
        }

        public Gender GetById(int id)
        {
            return _repository.FindById(id);
        }

        public Gender FindT(Expression<Func<Gender, bool>> predicad)
        {
            return _repository.FindT(predicad);
        }

    }
}
