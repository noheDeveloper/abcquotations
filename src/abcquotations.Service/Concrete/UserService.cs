﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using abcquotations.Domain.Entities;
using abcquotations.repository.Concrete;
using abcquotations.Service.Contract;

namespace abcquotations.Service.Concrete
{
    public class UserService :IUserService
    {
        private readonly Repository<User> _repository = new Repository<User>(new QuotationABCEntities());
        public void Add(User user)
        {
            _repository.Add(user);
            _repository.Save();
        }

        public void Update(User user)
        {
            _repository.Update(user);
            _repository.Save();
        }

        public void Delete(User user)
        {
            _repository.Delete(user);
            _repository.Save();
        }

        public IEnumerable<User> GetAll()
        {
            var list = _repository.GetAll().ToList();
            return list.Any() ? list : new List<User>();
        }

        public User GetById(int id)
        {
            return _repository.FindById(id);
        }

        public User FindT(Expression<Func<User, bool>> predicad)
        {
            return _repository.FindT(predicad);
        }
    }
}
