﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using abcquotations.Service.Contract;
using abcquotations.Web.Helpers;

namespace abcquotations.Web.Filters.Securiry
{
    public class AuthorizeRolesAttribute : AuthorizeAttribute
    {
        private readonly string[] _assignedRoles;
        public AuthorizeRolesAttribute(params string[] roles)
        {
            _assignedRoles = roles;
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var helper = new HelperFilter();
            return _assignedRoles.Select(roles => helper.IsUserInRole(httpContext.User.Identity.Name, roles)).Any(authorize => authorize);
        }
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new RedirectResult("~/Home/UnAuthorized");
        }
    }
}