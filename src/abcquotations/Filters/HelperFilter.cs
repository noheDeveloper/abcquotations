﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using abcquotations.Service.Concrete;
using abcquotations.Service.Contract;

namespace abcquotations.Web.Filters
{
    public class HelperFilter
    {
        private readonly IUserService _serviceUser;
        public HelperFilter()
        {
            _serviceUser = new UserService();
        }

        public bool IsUserInRole(string loginName, string roleName)
        {
            var user = _serviceUser.FindT(u => u.LoginName.ToLower().Equals(loginName));
            var userRole = (user?.UserRoles)?.FirstOrDefault(r => r.Role.Name.Equals(roleName));

            return userRole != null;
        }
    }
}