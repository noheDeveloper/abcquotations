﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using abcquotations.Domain.Entities;
using abcquotations.Service.Contract;
using abcquotations.Web.Filters.Securiry;
using abcquotations.Web.Helpers;
using abcquotations.Web.Models;
using AutoMapper;

namespace abcquotations.Web.Controllers
{
    [Authorize]
    public class ClientController : Controller
    {
        private readonly IClientService _clientService;
        private readonly IWayPayService _wayPayService;
        private readonly IUserService _userService;
        private readonly ITypeDocumentService _typeDocumentService;
        private readonly HelperProvider _helperProvider;
        private readonly HelperClient _helperClient;

        public ClientController(IClientService clientService, IWayPayService wayPayService, IContactService contactService, IUserService userService, ITypeDocumentService typeDocumentService)
        {
            _clientService = clientService;
            _helperProvider = new HelperProvider(wayPayService);
            _helperClient = new HelperClient(typeDocumentService);
            _userService = userService;
            
        }

        // GET: Client
        public ActionResult Index()
        {
            Mapper.Initialize(o => o.CreateMap<Client, ClientModel>());
            var clients = Mapper.Map<IEnumerable<Client>, List<ClientModel>>(_clientService.GetAll());
            return View(clients);
        }

        // GET: Client/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Client/Create
        [AuthorizeRoles("Admin", "Editor")]
        public ActionResult Create()
        {
            ViewData["SelectListWayPay"] = _helperProvider.ListWayPays();
            ViewData["SelectListTipeDoc"] = _helperClient.SelectListTypeDocuments();
            return View();
        }

        // POST: Client/Create
        [HttpPost]
        [AuthorizeRoles("Admin","Editor")]
        public ActionResult Create(ClientModel model, string[] selectedWayPay)
        {
            try
            {
                var currentUser = _userService.FindT(u => u.LoginName.Equals(User.Identity.Name));
                var client = new Client
                {
                    Consecutive = model.Consecutive,
                    IdTypeDocument = model.IdTypeDocument,
                    NumberDocument = model.NumberDocument,
                    RowCreatedDateTime = DateTime.Now,
                    RowCreatedSYSUserID = currentUser.Id,
                    Contact = new Contact
                    {
                        Name = model.NameContact,
                        Address = model.Address,
                        Phone = model.Phone,
                        Email = model.Email,
                        RowCreatedSYSUserID = currentUser.Id,
                        RowCreatedDateTime = DateTime.Now
                    }
                };

                _clientService.Add(client);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Client/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Client/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Client/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Client/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
