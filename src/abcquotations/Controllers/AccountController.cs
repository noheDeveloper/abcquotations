﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using abcquotations.Domain.Entities;
using abcquotations.Service.Contract;
using abcquotations.Web.Filters.Securiry;
using abcquotations.Web.Models;
using abcquotations.Web.Helpers;

namespace abcquotations.Web.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private readonly IUserService _serviceUser;
        private readonly IRoleService _serviceRole;
        private readonly IGenderService _serviceGender;
        private readonly IStateUserService _serviceStateUser;
        private readonly IStatesRolesService _statesRolesService;
        private readonly AccountHelper _helper;
        public AccountController(IUserService userService, IRoleService roleService, IGenderService genderService, IStateUserService stateUserService, IStatesRolesService statesRolesService)
        {
            _serviceUser = userService;
            _serviceRole = roleService;
            _serviceGender = genderService;
            _serviceStateUser = stateUserService;
            _statesRolesService = statesRolesService;
            _helper = new AccountHelper(userService, roleService, genderService, stateUserService, statesRolesService);
        }

        [AllowAnonymous]
        public ActionResult LogIn()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult LogIn(LogIn logIn, string returnUrl)
        {
            if (!ModelState.IsValid) return View(logIn);
            var password = _helper.GetPasswordByLoginName(logIn.LoginName);
            if (string.IsNullOrEmpty(password))
                ModelState.AddModelError("", "El usuario o password son incorrectos");
            else
            {
                if (logIn.Password.Equals(password))
                {
                    FormsAuthentication.SetAuthCookie(logIn.LoginName,false);
                    return RedirectToAction("Index", "Home");
                }

                ModelState.AddModelError("", "The password provided is incorrect.");
            }

            return View(logIn);
        }

        [Authorize]
        public ActionResult SignOut()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

        [AuthorizeRoles("Admin")]
        public ActionResult NewUser()
        {
            ViewData["SelectListRoles"] = _helper.SelectListRoles();
            ViewData["SelectListGenders"] = _helper.SelectListGenders();
            return View();
        }

        [HttpPost]
        [AuthorizeRoles("Admin")]
        public ActionResult NewUser(NewUser userSingUp)
        {
            if (ModelState.IsValid)
            {
                var newUser = _helper.MapSingUpToUser(userSingUp);
                var user = _serviceUser.FindT(o => o.LoginName.Equals(userSingUp.LoginName));
                if (user == null)
                {
                    _serviceUser.Add(newUser);
                    FormsAuthentication.SetAuthCookie(userSingUp.FirstName, false);
                    return RedirectToAction("Welcome", "Home");
                }
                else
                    ModelState.AddModelError("", "Login Name already taken.");
            }
            return View();
        }
    }
}