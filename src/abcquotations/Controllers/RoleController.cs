﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using abcquotations.Domain.Entities;
using abcquotations.Service.Contract;
using abcquotations.Web.Filters.Securiry;
using abcquotations.Web.Helpers;
using abcquotations.Web.Models;
using AutoMapper;

namespace abcquotations.Web.Controllers
{
    [Authorize]
    public class RoleController : Controller
    {
        private readonly IUserService _serviceUser;
        private readonly IRoleService _serviceRole;
        private readonly AccountHelper _helper;

        public RoleController(IRoleService  serviceRole, IUserService userService, IGenderService genderService, IStateUserService stateUserService, IStatesRolesService statesRolesService)
        {
            _serviceUser = userService;
            _serviceRole = serviceRole;
            _helper = new AccountHelper(userService, serviceRole, genderService, stateUserService, statesRolesService);
        }
        // GET: Role
        public ActionResult Index()
        {
            Mapper.Initialize(o => o.CreateMap<Role, RoleModel>());
            var roles = Mapper.Map<IEnumerable<Role>, List<RoleModel>>(_serviceRole.GetAll());

            return View(roles);
        }

        // GET: Role/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Role/Create
        public ActionResult Create()
        {
            ViewData["SelectListStatesRole"] = _helper.SelectListStatesRoles();
            return View();
        }

        // POST: Role/Create
        [AuthorizeRoles("Admin")]
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                var currentUser = _serviceUser.FindT(u => u.LoginName.Equals(User.Identity.Name));
                var newRole = new Role
                {
                    Name = collection["Name"],
                    Description = collection["Description"],
                    State = Convert.ToInt32(collection["State"]),
                    RowCreatedByUserID = currentUser.Id,
                    RowCreatedDateTime = DateTime.Now
                };

                _serviceRole.Add(newRole);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Role/Edit/5
        [AuthorizeRoles("Admin")]
        public ActionResult Edit(int id)
        {
            Mapper.Initialize(o => o.CreateMap<Role, RoleModel>());
            var role = Mapper.Map<Role, RoleModel>(_serviceRole.GetById(id));
            ViewData["SelectListStatesRole"] = _helper.SelectListStatesRoles();
            return View(role);
        }

        // POST: Role/Edit/5
        [AuthorizeRoles("Admin")]
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                var currentUser = _serviceUser.FindT(u => u.LoginName.Equals(User.Identity.Name));
                var setRole = _serviceRole.GetById(Convert.ToInt32(collection["IdRole"]));
                setRole.Name = collection["Name"];
                setRole.Description = collection["Description"];
                setRole.State = Convert.ToInt32(collection["State"]);
                setRole.RowModifiedByUserID = currentUser.Id;
                setRole.RowModifiedDateTime = DateTime.Now;

                _serviceRole.Update(setRole);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Role/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Role/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
