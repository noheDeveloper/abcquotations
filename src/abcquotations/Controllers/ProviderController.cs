﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using abcquotations.Domain.Entities;
using abcquotations.Service.Contract;
using abcquotations.Web.Filters.Securiry;
using abcquotations.Web.Helpers;
using abcquotations.Web.Models;
using AutoMapper;

namespace abcquotations.Web.Controllers
{
    [Authorize]
    public class ProviderController : Controller
    {
        private readonly IProviderService _providerService;
        private readonly IContactService _contactService;
        private readonly IWayPayService _wayPayService;
        private readonly IUserService _userService;
        private readonly HelperProvider _helperProvider;

        public ProviderController(IProviderService providerService, IWayPayService wayPayService, IContactService contactService, IUserService userService)
        {
            _providerService = providerService;
            _helperProvider = new HelperProvider(wayPayService);
            _contactService = contactService;
            _userService = userService;
        }

        // GET: Provider
        public ActionResult Index()
        {
            Mapper.Initialize(o => o.CreateMap<Provider, ProviderModel>());
            var providers = Mapper.Map<IEnumerable<Provider>, List<ProviderModel>>(_providerService.GetAll());
            return View(providers);
        }

        // GET: Provider/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Provider/Create
        [AuthorizeRoles("Admin", "Editor")]
        public ActionResult Create()
        {
            ViewData["SelectListWayPay"] = _helperProvider.ListWayPays();
            return View();
        }

        // POST: Provider/Create
        [AuthorizeRoles("Admin","Editor")]
        [HttpPost]
        public ActionResult Create(ProviderModel model, string[] selectedWayPay)
        {
            try
            {
                var currentUser = _userService.FindT(u => u.LoginName.Equals(User.Identity.Name));
                var listWayPay = selectedWayPay.Select(item => new Provider_WayPay
                {
                    IdWayPay = Convert.ToInt32(item)
                }).ToList();

                var provider = new Provider
                {
                    Contact = new Contact
                    {
                        Name = model.NameContact,
                        Address = model.Address,
                        Phone = model.Phone,
                        Email = model.Email,
                        RowCreatedSYSUserID = currentUser.Id,
                        RowCreatedDateTime = DateTime.Now
                    },
                    Consecutive = model.Consecutive,
                    Name = model.Name,
                    RowCreatedDateTime = DateTime.Now,
                    RowCreatedSYSUserID = currentUser.Id
                };
                _providerService.Add(provider);

                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                var exep = ex.Message;
                return View();
            }
        }

        // GET: Provider/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Provider/Edit/5
        [HttpPost]
        [AuthorizeRoles("Admin", "Editor")]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Provider/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Provider/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
