﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using abcquotations.Domain.Entities;
using abcquotations.Service.Contract;
using abcquotations.Web.Filters.Securiry;
using abcquotations.Web.Models;
using AutoMapper;

namespace abcquotations.Web.Controllers
{
    [Authorize]
    public class WayPayController : Controller
    {

        private readonly IWayPayService _wayPayService;

        public WayPayController(IWayPayService wayPayService)
        {
            _wayPayService = wayPayService;
        }
        // GET: WayPay
        public ActionResult Index()
        {
            Mapper.Initialize(o => o.CreateMap<WayPay, WayPayModel>());
            var wayPays = Mapper.Map<IEnumerable<WayPay>, List<WayPayModel>>(_wayPayService.GetAll());
            return View(wayPays);
        }

        // GET: WayPay/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: WayPay/Create
        [AuthorizeRoles("Admin", "Editor")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: WayPay/Create
        [HttpPost]
        [AuthorizeRoles("Admin", "Editor")]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: WayPay/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: WayPay/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: WayPay/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: WayPay/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
