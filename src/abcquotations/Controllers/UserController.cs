﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using abcquotations.Domain.Entities;
using abcquotations.Service.Contract;
using abcquotations.Web.Filters.Securiry;
using abcquotations.Web.Helpers;
using abcquotations.Web.Models;
using AutoMapper;

namespace abcquotations.Web.Controllers
{
    [Authorize]
    public class UserController : Controller
    {
        private readonly IUserService _serviceUser;
        private readonly IRoleService _serviceRole;
        private readonly IGenderService _serviceGender;
        private readonly IStateUserService _serviceStateUser;
        private readonly IUserRoleService _userRoleService;
        private readonly IStatesRolesService _statesRolesService;
        private readonly AccountHelper _helper;
        private readonly UserHelper _userHelper;
        public UserController(IUserService userService, IRoleService roleService, IGenderService genderService, IStateUserService stateUserService, IUserRoleService userRoleService, IStatesRolesService statesRolesService)
        {
            _serviceUser = userService;
            _statesRolesService = statesRolesService;
            _helper = new AccountHelper(userService, roleService, genderService, stateUserService, statesRolesService);
            _userHelper = new UserHelper(userService, userRoleService);
            
        }
        // GET: User
        public ActionResult Index()
        {
            Mapper.Initialize(o => o.CreateMap<User, UserModel>());
            var users = Mapper.Map<IEnumerable<User>, List<UserModel>>(_serviceUser.GetAll());

            return View(users);
        }

        // GET: User/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: User/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: User/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: User/Edit/5
        [AuthorizeRoles("Admin")]
        public ActionResult Edit(int id)
        {
            Mapper.Initialize(o => o.CreateMap<User, UserModel>());
            var userBd = _serviceUser.GetById(id);
            var user = Mapper.Map<User, UserModel>(userBd);
            ViewData["SelectCurrentRoles"] = userBd.UserRoles.ToList();
            ViewData["SelectListRoles"] = _helper.SelectListRoles();
            ViewData["SelectListStates"] = _helper.SelectListStatesUser();
            return View(user);
        }

        // POST: User/Edit/5
        [AuthorizeRoles("Admin")]
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                var user = _userHelper.SetEditUser(collection);
                _serviceUser.Update(user);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: User/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: User/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
