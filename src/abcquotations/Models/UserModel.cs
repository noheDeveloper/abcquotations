﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace abcquotations.Web.Models
{
    public class UserModel
    {
        public UserModel()
        {
            Roles = new List<SelectListItem>();
            States = new List<SelectListItem>();
        }

        [Display(Name = "Id")]
        public int Id { get; set; }
        [Display(Name = "Nombre usuario")]
        public string LoginName { get; set; }
        [Display(Name = "Password")]
        public string PasswordEncrypted { get; set; }
        [Display(Name = "Estado")]
        public int State { get; set; }
        [Display(Name = "Rol")]
        public int RoleId { get; set; }
        public IEnumerable<SelectListItem> Roles { get; set; }

        public IEnumerable<SelectListItem> States { get; set; }
    }
}