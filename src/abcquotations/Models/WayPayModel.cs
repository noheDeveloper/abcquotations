﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using abcquotations.Domain.Entities;

namespace abcquotations.Web.Models
{
    public class WayPayModel
    {
        public int IdWayPay { get; set; }
        public string Days { get; set; }
        public string Description { get; set; }

    }

    
}