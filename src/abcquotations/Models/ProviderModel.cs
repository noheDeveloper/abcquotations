﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using abcquotations.Domain.Entities;

namespace abcquotations.Web.Models
{
    public class ProviderModel
    {
        public ProviderModel()
        {
            WayPays = new List<WayPay>();
        }

        [Required(ErrorMessage = "Consecutivo es requerido !")]
        [Display(Name = "Consecutivo Proveedora")]
        public string Consecutive { get; set; }
        [Required(ErrorMessage = "Nombre Entidad es requerido !")]
        [Display(Name = "Nombre Entidad Proveedora")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Nombre Contacto es requerido !")]
        [Display(Name = "Nombre Contacto")]
        public string NameContact { get; set; }
        [Required(ErrorMessage = "Dirección es requerido !")]
        [Display(Name = "Dirección")]
        public string Address { get; set; }
        [DataType(DataType.PhoneNumber)]
        [Required(ErrorMessage = "Teléfono es requerido !")]
        [Display(Name = "Teléfono")]
        public string Phone { get; set; }
        [DataType(DataType.EmailAddress)]
        [Required(ErrorMessage = "Email es requerido !")]
        [Display(Name = "Email")]
        public string Email { get; set; }
        [Display(Name = "Modos de Pago")]
        public IEnumerable<WayPay> WayPays { get; set; }

        public Contact Contact { get; set; }

    }
}