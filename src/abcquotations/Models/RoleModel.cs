﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace abcquotations.Web.Models
{
    public class RoleModel
    {
        [Display(Name = "Id")]
        public int IdRole { get; set; }
        [Required(ErrorMessage = "Nombre es requerido !")]
        [Display(Name = "Nombre Rol")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Descripción es requerida !")]
        [Display(Name = "Descripción")]
        public string Description { get; set; }
        [Required(ErrorMessage = "Estado es requerida !")]
        [Display(Name = "Estado")]
        public int State { get; set; }
    }
}