﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace abcquotations.Web.Models
{
    public class LogIn
    {
        [Key]
        public int UserId { get; set; }
        [Required(ErrorMessage = "LoginName es requerido !")]
        [Display(Name = "LoginName")]
        public string LoginName { get; set; }
        [DataType(DataType.Password)]
        //[StringLength(20, MinimumLength = 8)]
        //[RegularExpression("/^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$/",ErrorMessage = "at least 1 number and 1 character")]
        [Display(Name = "Password")]
        public string Password { get; set; }
    }
}