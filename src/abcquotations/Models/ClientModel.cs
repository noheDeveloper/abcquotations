﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using abcquotations.Domain.Entities;

namespace abcquotations.Web.Models
{
    public class ClientModel
    {
        public ClientModel()
        {
            TypesDocuments = new List<SelectListItem>();
            WayPays = new List<WayPay>();
        }

        public int IdClient { get; set; }
        [Required(ErrorMessage = "Consecutivo es requerido !")]
        [Display(Name = "Consecutivo Cliente")]
        public string Consecutive { get; set; }
        public int IdWayPay { get; set; }
        public int IdTypeDocument { get; set; }
        [Required(ErrorMessage = "Numero de documento es requerido !")]
        [Display(Name = "Numero de documento Cliente")]
        public string NumberDocument { get; set; }
        public int IdContact { get; set; }
        [Required(ErrorMessage = "Nombre Contacto es requerido !")]
        [Display(Name = "Nombre Contacto")]
        public string NameContact { get; set; }
        [Required(ErrorMessage = "Dirección es requerido !")]
        [Display(Name = "Dirección")]
        public string Address { get; set; }
        [DataType(DataType.PhoneNumber)]
        [Required(ErrorMessage = "Teléfono es requerido !")]
        [Display(Name = "Teléfono")]
        public string Phone { get; set; }
        [DataType(DataType.EmailAddress)]
        [Required(ErrorMessage = "Email es requerido !")]
        [Display(Name = "Email")]
        public string Email { get; set; }
        [Display(Name = "Modos de Pago")]
        public IEnumerable<WayPay> WayPays { get; set; }

        public IEnumerable<SelectListItem> TypesDocuments { get; set; }
        public Contact Contact { get; set; }
    }
}