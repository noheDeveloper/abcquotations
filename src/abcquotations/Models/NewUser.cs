﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using abcquotations.Domain.Entities;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace abcquotations.Web.Models
{
    public class NewUser
    {
        public NewUser()
        {
            Roles = new List<SelectListItem>();
            Genders = new List<SelectListItem>();
        }

        public int Id { get; set; }
        [Required(ErrorMessage = "LoginName es requerido !")]
        [Display(Name = "LoginName")]
        public string LoginName { get; set; }
        [DataType(DataType.Password)]
        //[StringLength(20, MinimumLength = 8)]
        //[RegularExpression("/^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$/",ErrorMessage = "at least 1 number and 1 character")]
        [Display(Name = "Password")]
        [Required(ErrorMessage = "Password es requerido !")]
        public string PasswordEncrypted { get; set; }
        [Required(ErrorMessage = "Nombre es requerido !")]
        [Display(Name = "Nombre")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Apellido es requerido !")]
        [Display(Name = "Apellido")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "Rol es requerido !")]
        [Display(Name = "Rol")]
        public int IdRol { get; set; }
        [Required(ErrorMessage = "Genero es requerido !")]
        [Display(Name = "Genero")]
        public int IdGender { get; set; }
        [DataType(DataType.EmailAddress)]
        [Required(ErrorMessage = "Email es requerido !")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        public IEnumerable<SelectListItem> Genders { get; set; }
        public IEnumerable<SelectListItem> Roles { get; set; }
    }
}