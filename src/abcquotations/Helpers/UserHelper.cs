﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using abcquotations.Domain.Entities;
using abcquotations.Service.Contract;

namespace abcquotations.Web.Helpers
{
    public class UserHelper
    {
        private readonly IUserService _serviceUser;
        private readonly IUserRoleService _userRoleService;

        public UserHelper(IUserService serviceUser, IUserRoleService userRoleService)
        {
            _serviceUser = serviceUser;
            _userRoleService = userRoleService;
        }

        public User SetEditUser(FormCollection collection)
        {
            var currentUser = _serviceUser.FindT(u => u.LoginName.Equals(HttpContext.Current.User.Identity.Name));
            var setUser = _serviceUser.GetById(Convert.ToInt32(collection["Id"]));
            setUser.Id = Convert.ToInt32(collection["Id"]);
            setUser.LoginName = collection["LoginName"];
            setUser.PasswordEncrypted = collection["PasswordEncrypted"];
            setUser.State = Convert.ToInt32(collection["State"]);
            setUser.RowModifiedByUserID = currentUser.Id;
            setUser.RowModifiedDateTime = DateTime.Now;

            var roleId = string.IsNullOrEmpty(collection["RoleId"]) ? 0 : Convert.ToInt32(collection["RoleId"]);

            if (roleId <= 0) return setUser;
            if (setUser.UserRoles.Any(o => o.RoleID.Equals(roleId))) return setUser;

            var newUserRole = new UserRole()
            {
                UserID = setUser.Id,
                RoleID = roleId,
                State = 1,
                RowCreatedByUserID = currentUser.Id,
                RowCreatedDateTime = DateTime.Now,
            };
            _userRoleService.Add(newUserRole);
            return setUser;
        }
    }
}