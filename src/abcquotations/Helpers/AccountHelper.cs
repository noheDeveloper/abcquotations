﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using abcquotations.Domain.Entities;
using abcquotations.Service.Contract;
using abcquotations.Web.Models;

namespace abcquotations.Web.Helpers
{
    public class AccountHelper
    {
        private readonly IUserService _serviceUser;
        private readonly IRoleService _serviceRole;
        private readonly IGenderService _serviceGender;
        private readonly IStateUserService _serviceStateUser;
        private readonly IStatesRolesService _statesRolesService;

        public AccountHelper(IUserService userService, IRoleService roleService, IGenderService genderService, IStateUserService serviceStateUser, IStatesRolesService statesRolesService)
        {
            _serviceUser = userService;
            _serviceRole = roleService;
            _serviceGender = genderService;
            _serviceStateUser = serviceStateUser;
            _statesRolesService = statesRolesService;
        }

        public User MapSingUpToUser(NewUser userSingUp)
        {
            var profiles = new List<Profile>
            {
                new Profile
                {
                    FirstName = userSingUp.FirstName,
                    GenderId = userSingUp.IdGender,
                    LastName = userSingUp.LastName,
                    RowCreatedByUserID = 1,
                    RowCreatedDateTime = DateTime.Now,
                    Email = userSingUp.Email
                }
            };

            var userRole = new List<UserRole>
            {
                new UserRole
                {
                    RoleID = userSingUp.IdRol,
                    RowCreatedByUserID = 1,
                    RowCreatedDateTime = DateTime.Now,
                    State = 1
                }
            };

            var user = new User
            {
                LoginName = userSingUp.LoginName,
                PasswordEncrypted = userSingUp.PasswordEncrypted,
                RowCreatedByUserID = 1,
                Profiles = profiles,
                UserRoles = userRole,
                RowCreatedDateTime = DateTime.Now,
                State = 1
            };

            return user;

        }

        public string GetPasswordByLoginName(string loginName)
        {
            var user = _serviceUser.FindT(u => u.LoginName.ToLower().Equals(loginName));
            return user != null ? user.PasswordEncrypted : string.Empty;
        }
        
        public IEnumerable<SelectListItem> SelectListRoles()
        {
            var roles = _serviceRole.GetAll().Select( x=> new SelectListItem
            {
                Value = x.IdRole.ToString(),
                Text = x.Name,
                Selected = true,
            });

            return new SelectList(roles, "Value", "Text");
        }

        public IEnumerable<SelectListItem> SelectListGenders()
        {
            var roles = _serviceGender.GetAll().Select(x => new SelectListItem
            {
                Value = x.IdGender.ToString(),
                Text = x.Description,
                Selected = true,
            });

            return new SelectList(roles, "Value", "Text");
        }

        public IEnumerable<SelectListItem> SelectListStatesUser()
        {
            var roles = _serviceStateUser.GetAll().Select(x => new SelectListItem
            {
                Value = x.IdStateUser.ToString(),
                Text = x.Description,
                Selected = true,
            });

            return new SelectList(roles, "Value", "Text");
        }

        public IEnumerable<SelectListItem> SelectListStatesRoles()
        {
            var roles = _statesRolesService.GetAll().Select(x => new SelectListItem
            {
                Value = x.IdStateRol.ToString(),
                Text = x.Description,
                Selected = true,
            });

            return new SelectList(roles, "Value", "Text");
        }
    }
}