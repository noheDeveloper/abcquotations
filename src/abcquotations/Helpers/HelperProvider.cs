﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using abcquotations.Domain.Entities;
using abcquotations.Service.Contract;

namespace abcquotations.Web.Helpers
{
    public class HelperProvider
    {
        //private readonly IWayPayService _wayPayService;
        private readonly IWayPayService _wayPayService;
        public HelperProvider(IWayPayService wayPayService)
        {
            _wayPayService = wayPayService;
        }
        public IEnumerable<WayPay> ListWayPays()
        {
            var wayPays = _wayPayService.GetAll().ToList();
            return wayPays;
        }
    }
}
