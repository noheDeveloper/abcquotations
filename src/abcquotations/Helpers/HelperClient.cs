﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using abcquotations.Service.Contract;

namespace abcquotations.Web.Helpers
{
    public class HelperClient
    {
        private readonly ITypeDocumentService _typeDocumentService;

        public HelperClient(ITypeDocumentService typeDocumentService)
        {
            _typeDocumentService = typeDocumentService;
        }

        public IEnumerable<SelectListItem> SelectListTypeDocuments()
        {
            var roles = _typeDocumentService.GetAll().Select(x => new SelectListItem
            {
                Value = x.IdTypeDocument.ToString(),
                Text = x.Description,
                Selected = true,
            });

            return new SelectList(roles, "Value", "Text");
        }
    }
}